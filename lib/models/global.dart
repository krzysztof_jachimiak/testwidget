import 'package:flutter/material.dart';
// import 'user.dart';
// import 'post.dart';
// import 'comment.dart';

//  - lib/assets/photo4.jpg
//  - lib/assets/photo5.jpg
//  - lib/assets/profile3.png
//  - lib/assets/profile4.png
//  - lib/assets/profile6.jpg

TextStyle textStyle = new TextStyle(fontFamily: 'Gotham');
TextStyle textStyleBold = new TextStyle(
    fontFamily: 'Gotham', fontWeight: FontWeight.bold, color: Colors.black);
TextStyle textStyleLigthGrey =
    new TextStyle(fontFamily: 'Gotham', color: Colors.grey);
// AppbarParams appBarParams = new AppbarParams("Instagram", []);

// Post post1 = new Post(
//     new AssetImage('lib/assets/post.jpg'),
//     user,
//     "My first post",
//     DateTime.now(),
//     [follower1, follower2, follower3],
//     [],
//     false,
//     false);
// final User user = new User(
//     'Messi',
//     AssetImage('lib/assets/profil.jpg'),
//     [follower1, follower2, follower3],
//     [follower1, follower2, follower3, follower4, follower5, follower6],
//     [],
//     [],
//     false);
// User follower1 = new User(
//     'pedri', AssetImage('lib/assets/profil2.jpg'), [], [], [], [], true);
// User follower2 = new User(
//     'iniesta', AssetImage('lib/assets/profil3.jpg'), [], [], [], [], false);
// User follower3 = new User(
//     'busquets', AssetImage('lib/assets/profil4.jpg'), [], [], [], [], true);
// User follower4 = new User(
//     'pique', AssetImage('lib/assets/profil5.jpg'), [], [], [], [], true);
// User follower5 = new User(
//     'frankie', AssetImage('lib/assets/profil4.png'), [], [], [], [], true);
// User follower6 = new User(
//     'busi', AssetImage('lib/assets/profil4.jpg'), [], [], [], [], false);
// List<Post> userPosts = [
//   new Post(
//       new AssetImage('lib/assets/post2.jpg'),
//       user,
//       "My first post",
//       DateTime.now(),
//       [follower1, follower2, follower3, follower4, follower5, follower6],
//       [
//         new Comment(follower1, "This was amazing!", DateTime.now(), false),
//         new Comment(follower2, "Cool one", DateTime.now(), false),
//         new Comment(
//             follower4,
//             "This is no good at all \nbuddy, don't post this stuff",
//             DateTime.now(),
//             false)
//       ],
//       false,
//       false),
//   new Post(
//       new AssetImage('lib/assets/post3.jpg'),
//       follower1,
//       "This is such a great post though",
//       DateTime.now(),
//       [user, follower2, follower3, follower4, follower5],
//       [
//         new Comment(follower3, "This was super cool!", DateTime.now(), false),
//         new Comment(follower1, "I can't believe it's not \nbutter!",
//             DateTime.now(), false),
//         new Comment(user, "I know rite!", DateTime.now(), false),
//         new Comment(follower5, "I'm batman", DateTime.now(), false)
//       ],
//       false,
//       false),
//   new Post(
//       new AssetImage('lib/assets/post4.png'),
//       follower5,
//       "How did I even take this photo??",
//       DateTime.now(),
//       [user, follower2, follower3, follower4, follower5],
//       [
//         new Comment(follower3, "This was super cool!", DateTime.now(), false),
//         new Comment(follower1, "I can't believe it's not \nbutter!",
//             DateTime.now(), false),
//         new Comment(user, "I know rite!", DateTime.now(), false),
//         new Comment(follower5, "I'm batman", DateTime.now(), false)
//       ],
//       false,
//       false),
//   new Post(
//       new AssetImage('lib/assets/post5.png'),
//       follower3,
//       "Found this in my backyard. \nThought I'd post it jk lol lol lolol",
//       DateTime.now(),
//       [user, follower2, follower3, follower4, follower5],
//       [
//         new Comment(follower3, "This was super cool!", DateTime.now(), false),
//         new Comment(follower1, "I can't believe it's not \nbutter!",
//             DateTime.now(), false),
//         new Comment(user, "I know rite!", DateTime.now(), false),
//         new Comment(follower5, "I'm batman", DateTime.now(), false)
//       ],
//       false,
//       false),
//   new Post(
//       new AssetImage('lib/assets/post4.png'),
//       follower3,
//       "Found this in my backyard. \nThought I'd post it jk lol lol lolol",
//       DateTime.now(),
//       [user, follower2, follower3, follower4, follower5],
//       [
//         new Comment(follower3, "This was super cool!", DateTime.now(), false),
//         new Comment(follower1, "I can't believe it's not \nbutter!",
//             DateTime.now(), false),
//         new Comment(user, "I know rite!", DateTime.now(), false),
//         new Comment(follower5, "I'm batman", DateTime.now(), false)
//       ],
//       false,
//       false),
//   new Post(
//       new AssetImage('lib/assets/post3.jpg'),
//       follower3,
//       "Found this in my backyard. \nThought I'd post it jk lol lol lolol",
//       DateTime.now(),
//       [user, follower2, follower3, follower4, follower5],
//       [
//         new Comment(follower3, "This was super cool!", DateTime.now(), false),
//         new Comment(follower1, "I can't believe it's not \nbutter!",
//             DateTime.now(), false),
//         new Comment(user, "I know rite!", DateTime.now(), false),
//         new Comment(follower5, "I'm batman", DateTime.now(), false)
//       ],
//       false,
//       false),
//   new Post(
//       new AssetImage('lib/assets/post2.jpg'),
//       follower3,
//       "Found this in my backyard. \nThought I'd post it jk lol lol lolol",
//       DateTime.now(),
//       [user, follower2, follower3, follower4, follower5],
//       [
//         new Comment(follower3, "This was super cool!", DateTime.now(), false),
//         new Comment(follower1, "I can't believe it's not \nbutter!",
//             DateTime.now(), false),
//         new Comment(user, "I know rite!", DateTime.now(), false),
//         new Comment(follower5, "I'm batman", DateTime.now(), false)
//       ],
//       false,
//       false),
//   new Post(
//       new AssetImage('lib/assets/post4.png'),
//       follower3,
//       "Found this in my backyard. \nThought I'd post it jk lol lol lolol",
//       DateTime.now(),
//       [user, follower2, follower3, follower4, follower5],
//       [
//         new Comment(follower3, "This was super cool!", DateTime.now(), false),
//         new Comment(follower1, "I can't believe it's not \nbutter!",
//             DateTime.now(), false),
//         new Comment(user, "I know rite!", DateTime.now(), false),
//         new Comment(follower5, "I'm batman", DateTime.now(), false)
//       ],
//       false,
//       false),
//   new Post(
//       new AssetImage('lib/assets/post5.png'),
//       follower1,
//       "Found this in my backyard. \nThought I'd post it jk lol lol lolol",
//       DateTime.now(),
//       [user, follower2, follower3, follower4, follower5],
//       [
//         new Comment(follower3, "This was super cool!", DateTime.now(), false),
//         new Comment(follower1, "I can't believe it's not \nbutter!",
//             DateTime.now(), false),
//         new Comment(user, "I know rite!", DateTime.now(), false),
//         new Comment(follower5, "I'm batman", DateTime.now(), false)
//       ],
//       false,
//       false),
// ];

// String title = "Instagram";
